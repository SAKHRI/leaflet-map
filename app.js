// var ny = [40.7127281, -74.0060152];
// creation de la map
const mymap = L.map('map').setView([0, 0], 1);
const marker = L.marker([0, 0]).addTo(mymap);


//creation du calque images
const attribution = '&copy; <a href="https://www.openstreetmap.org/coryright">OpenStreetMap</a>';
const tileUrl = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
const tiles = L.tileLayer(tileUrl, { attribution });
tiles.addTo(mymap)

const api_url = 'https://api.wheretheiss.at/v1/satellites/25544';

async function getISS() {
    const response = await fetch(api_url);
    const data = await response.json();
    const { latitude, longitude } = data;

    // L.marker([latitude, longitude]).addTo(mymap);

    marker.setLatLng([latitude, longitude]);

    document.getElementById('lat').textContent = latitude;
    document.getElementById('lon').textContent = longitude;
}

marker.bindPopup('<h3> Satellite : ISS </h3>')

getISS();

